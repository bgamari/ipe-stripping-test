#include <stdio.h>

extern int test;

int main() {
    // TODO: This is very fragile as C semantics
    // allow the compiler to assume that a variable
    // always has non-NULL storage.
    int *t = &test;
    printf("%p\n", t);

    if (t != NULL) {
        printf("%d\n", test);
    } else {
        printf("nope\n");
    }

    return 0;
}
