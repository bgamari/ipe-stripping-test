#!/usr/bin/env bash

set -e

gcc -c test.c
gcc -c -g -O0 main.c
objdump -x test.o

objcopy --strip-symbol=test --remove-section='.data$ipe' --add-symbol test=0 test.o
objdump -x test.o

gcc test.o main.o -o main
./main
